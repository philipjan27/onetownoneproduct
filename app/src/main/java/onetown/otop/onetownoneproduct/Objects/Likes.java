package onetown.otop.onetownoneproduct.Objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by EasyBreezy on 12/27/2016.
 */

public class Likes implements Parcelable {
    public int _id;
    public int liked;
    public Credentials credentials;
    public LocationsData locationsData;

public Likes() {
    super();
}

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getLiked() {
        return liked;
    }

    public void setLiked(int liked) {
        this.liked = liked;
    }

    public LocationsData getLocationsData() {
        return locationsData;
    }

    public void setLocationsData(LocationsData locationsData) {
        this.locationsData = locationsData;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public static Creator<Likes> getCREATOR() {
        return CREATOR;
    }

    protected Likes(Parcel in) {
        _id = in.readInt();
        liked = in.readInt();
        credentials = in.readParcelable(Credentials.class.getClassLoader());
        locationsData = in.readParcelable(LocationsData.class.getClassLoader());
    }

    public static final Creator<Likes> CREATOR = new Creator<Likes>() {
        @Override
        public Likes createFromParcel(Parcel in) {
            return new Likes(in);
        }

        @Override
        public Likes[] newArray(int size) {
            return new Likes[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(_id);
        parcel.writeInt(liked);
        parcel.writeParcelable(credentials, i);
        parcel.writeParcelable(locationsData, i);
    }
}
