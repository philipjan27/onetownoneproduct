package onetown.otop.onetownoneproduct.Classes;

/**
 * Created by EasyBreezy on 1/16/2017.
 */

public class OnCheckBoxAlreadyLike {


    public interface OnCheckBoxAlreadyLikeListener {

        public void onChkbxAlreadyChecked(boolean alreadyChecked);
    }

    private OnCheckBoxAlreadyLikeListener listener;

    public OnCheckBoxAlreadyLike() {
        this.listener=null;
    }

    public void setCustomOnChkbkxListener(OnCheckBoxAlreadyLikeListener listener) {
        this.listener= listener;
    }
}
