package onetown.otop.onetownoneproduct.Activity;


import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;

import onetown.otop.onetownoneproduct.Classes.OnCheckBoxAlreadyLike;
import onetown.otop.onetownoneproduct.Database.DBHelper;
import onetown.otop.onetownoneproduct.Objects.Comments;
import onetown.otop.onetownoneproduct.Objects.Credentials;
import onetown.otop.onetownoneproduct.Objects.Likes;
import onetown.otop.onetownoneproduct.Objects.LocationsData;
import onetown.otop.onetownoneproduct.R;

public class PlaceDetailsActivity extends AppCompatActivity {

    private final String TAG= "PlaceDetailsActivity";
    private TextView placeDetailstextview;
    private ImageView placeImageView;
    private EditText commentEditext;
    private Button postComment;
    private ListView commentsListView;
    CheckBox likedChkbx;
    TextView placesLiked;
    Likes likes;
    LinearLayout customLinearLayout;
    DBHelper helper;
    LocationsData locationsData,data1;
    Comments comments;
    Credentials credentials,cred;
    ArrayList<Comments> commentsArrayList=  new ArrayList<Comments>();
    ArrayList<View> textViewArrayList= new ArrayList<>();
    long _userId;
    int checkboxChecked=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_details_new1);

        Toolbar toolbar= (Toolbar)findViewById(R.id.placedetails_toolbar);
        setSupportActionBar(toolbar);

        helper= new DBHelper(PlaceDetailsActivity.this);

        credentials= new Credentials();
        locationsData=getIntent().getExtras().getParcelable("places");
        Log.i("LocationsData",locationsData.toString());

        // Shared Prefs
        SharedPreferences userIdPref= getSharedPreferences("useridpref",0);
        _userId=userIdPref.getLong("user_id",0);

        customLinearLayout= (LinearLayout)findViewById(R.id.dynamicLayout_linearLayout);

        placeDetailstextview= (TextView)findViewById(R.id.textview_placeDetails);
        placeDetailstextview.setText(locationsData.getLocationProducts());
        placeImageView= (ImageView)findViewById(R.id.place_image);
        placeImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        placeImageView.setImageResource(Integer.parseInt(locationsData.getImage_path()));
        commentEditext= (EditText)findViewById(R.id.comment_editext);
        postComment= (Button)findViewById(R.id.button_postComment);
        commentsListView= (ListView)findViewById(R.id.comments_listview);
        likedChkbx= (CheckBox) findViewById(R.id.like_button);
        placesLiked= (TextView)findViewById(R.id.textvIew_userLiked);

        commentsArrayList.clear();
        if (commentsArrayList.isEmpty()) {
            commentsArrayList=helper.queryCommentsInEveryOtop(locationsData.get_id());
            AddCommentsToLinearLayout();
        }else {

            customLinearLayout.removeAllViews();
            AddCommentsToLinearLayout();
        }


        postComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                comments= new Comments();
                comments.setCommentContent(commentEditext.getText().toString());
                comments.setCurrentTimeStamp(helper.getCurrentTimeStamp());

               data1= new LocationsData();
                data1.set_id(locationsData.get_id());
                comments.setLocationsData(data1);

                cred= new Credentials();
                cred.set_id(_userId);
                comments.setCredentials(cred);

                Log.d("CommentsValue",comments.toString());

                if (commentEditext.getText().toString().length() == 0 || commentEditext.getText().toString() == null) {
                    Toast.makeText(getApplicationContext(),"Empty values are not allowed.",Toast.LENGTH_LONG).show();
                }else {
                    helper.addComment(comments);
                    commentEditext.setText("");
                    customLinearLayout.removeAllViews();
                    commentsArrayList.clear();
                    commentsArrayList=helper.queryCommentsInEveryOtop(data1.get_id());
                    AddCommentsToLinearLayout();
                }

            }

        });

        /** Instantiation of Objects *
         * CHECK IF USER ALREADY LIKED THE PLACE, and SET checkbox to CHECKED.
         */
        likes= new Likes();
        Credentials userLiked= new Credentials();
        userLiked.set_id(_userId);
        LocationsData placesLike=new LocationsData();
        placesLike.set_id(locationsData.get_id());

        likes.setLiked(checkboxChecked);
        likes.setLocationsData(placesLike);
        likes.setCredentials(userLiked);

        boolean  verifyIfPlaceLiked=helper.checkIfCurrentuserLikedPlace(likes.getCredentials().get_id(),likes.getLocationsData().get_id());
        if (verifyIfPlaceLiked == true) {
            likedChkbx.setChecked(true);
            placesLiked.setText("You liked this");
            Log.d("Check if liked or not","Already Checked ? "+verifyIfPlaceLiked);
           // Toast.makeText(getApplicationContext(),"Place already liked",Toast.LENGTH_SHORT).show();

        }else {
            likedChkbx.setChecked(false);
        }

        /** Checkbox Listener */
        likedChkbx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (likedChkbx.isChecked()) {
                    placesLiked.setText("You liked this");
                    helper.addLikeToOtop(likes);

                }else {
                    placesLiked.setText("");
                    likedChkbx.setChecked(false);
                }


    }
});

    } // End of OnCreate

    @Override
    protected void onResume() {
        super.onResume();

    }

    // Adding Comments into the Comments LinearLayout instead of ListView
    public void AddCommentsToLinearLayout() {

       int arraySize= commentsArrayList.size();
        for (int i=0; i < arraySize; i++) {

            View customLayout= new View(PlaceDetailsActivity.this);
            customLayout= getLayoutInflater().inflate(R.layout.customcomment_layout,null);
            TextView  email= (TextView)customLayout.findViewById(R.id.customlistview_username);
            TextView comment= (TextView)customLayout.findViewById(R.id.customlistview_commentcontent);
            TextView timestamp= (TextView)customLayout.findViewById(R.id.customlistview_timestamp);


            email.setText(commentsArrayList.get(i).getCredentials().getEmail());
            comment.setText(commentsArrayList.get(i).getCommentContent());
            timestamp.setText(commentsArrayList.get(i).getCurrentTimeStamp());

            customLinearLayout.addView(customLayout);
           textViewArrayList.add(customLayout);

        }
    }
}
