package onetown.otop.onetownoneproduct.Classes;

import onetown.otop.onetownoneproduct.Database.DBHelper;
import onetown.otop.onetownoneproduct.Objects.LocationsData;
import onetown.otop.onetownoneproduct.R;

/**
 * Created by root on 11/20/16.

 import onetown.otop.onetownoneproduct.Activity.MainActivity;
 */

public class LocationStaticDatas {

    public LocationStaticDatas(DBHelper helper) {

        // Region 11
        helper.addNewLocation(new LocationsData("Davao City","Agriculture remains the largest economic sector comprising banana, pineapple, coffee and coconut plantations in the city. " +
                "Bearing the nickname as the Fruit Basket of the Philippines, it is the island's leading exporter of fruits such as mangoes, pomeloes, bananas, coconut products, pineapples, papayas, mangosteens and cacao.",7.253501,125.1708687, String.valueOf(R.drawable.davao)));
        helper.addNewLocation(new LocationsData("Davao Del Sur","Davao del Sur (Cebuano: Habagatang Dabaw, Tagalog: Timog Dabaw) is a province in the Philippines located in the Davao Region in Mindanao. " +
                "Its capital and largest city is Digos. The province is bordered by Davao City to the north, Davao Occidental to the south and Cotabato, Sultan Kudarat, South Cotabato and Sarangani to the west. To the east lies the Davao Gulf.",6.4653752,124.2872263,String.valueOf(R.drawable.davaocity)));
        helper.addNewLocation(new LocationsData("Davao Del Norte","Davao del Norte (Cebuano: Amihanang Dabaw, Filipino: Hilagang Dabaw), is a province in the Philippines located in the Davao Region in Mindanao. Its capital is Tagum City. Davao del Norte also includes Samal Island to the south in the Davao Gulf." +
                "Before 1967, the five provinces — Davao del Norte, Davao Oriental, Davao del Sur, Davao Occidental and Compostela Valley—were once a single province named Davao. The Davao Region covers this historic province." +
                "Davao del Norte is also known as the banana capital of the Philippines. The principal crops of the province include rice, maize, banana, coconut, abacá, ramie, coffee, and a variety of fruit and root crops. Davao del Norte is the country's leading producer of bananas, " +
                "with many plantations run by multinationals Dole and Del Monte, and local producers such as Lapanday, TADECO, and Marsman. Davao del Norte is also one of Mindanao's leading producer of rice.",7.4455742,125.036961,String.valueOf(R.drawable.davaocity)));
        helper.addNewLocation(new LocationsData("Davao Oriental","Davao Oriental (Cebuano: Sidlakang Dabaw, Filipino: Silangang Dabaw) is a province in the Philippines located in the Davao Region in Mindanao. Its capital is Mati and borders Compostela Valley to the west," +
                " and Agusan del Sur and Surigao del Sur to the north." +
                "Davao Oriental is the easternmost province of the country with Pusan Point as the easternmost location. The Philippine Sea, part of the Pacific Ocean, faces Davao Oriental to the east." +
                " Part of the province lies on an unnamed peninsula that encloses the Davao Gulf to the west.",7.1382151,125.1483339,String.valueOf(R.drawable.davaocity)));
        helper.addNewLocation(new LocationsData("Compostela Valley","The main sources of livelihood are agricultural products such as rice, coconut, cacao, coffee, papaya, mango, pineapple, durian and banana. It has been projected that by 2030," +
                " the province will be one of the richest provinces in the country because of its rich natural resources and hardworking people. Some residents have fishponds and culture their own fish like tilapia and milkfish. The province is also rich with gold ore. Nabunturan," +
                " the provincial capital, is home to the biggest gold ring in the Philippines, The Solidarity Ring.",7.5384869,125.4260916,String.valueOf(R.drawable.davaocity)));
        helper.addNewLocation(new LocationsData("Sarangani","Coconut, corn, rice, banana, mango, durian, rubber, and sugarcane are major crops now being planted by the inhabitants. The province has plantations (mango, banana, pineapple, asparagus), cattle ranches, and commercial" +
                " fishponds that have been operating in the area, some of which having existed as far back as 40 years.",6.0171631,124.3854389,String.valueOf(R.drawable.davaocity)));
    }




}
